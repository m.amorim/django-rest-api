from datetime import timedelta

from django.utils import timezone
from django.db.models import Sum

from analytics.models import UserVisit
from analytics.utils import log_user_visit

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView


class HelloWorld(APIView):
    """
    Basic 'Hello World' view. Show our current API version, the current time, the number of recent visitors
    in the last 1 hour, and the total number of visitors and page visits
    """

    def get(self, request, format=None):
        log_user_visit(request.user)

        recent_timestamp = timezone.now() - timedelta(hours=1)
        recent_visitors = UserVisit.objects.filter(last_seen__gt=recent_timestamp).count()
        sum_visits = UserVisit.objects.aggregate(all_visits=Sum('visits'))
        data = {
            'version': 1.0,
            'time': timezone.now(),
            'recent_visitors': recent_visitors,
            'all_visitors': UserVisit.objects.all().count(),
            'all_visits': sum_visits.get("all_visits", 0),
        }
        return Response(data)

