from django.utils import timezone
from django.contrib.auth.models import User

from analytics.models import UserVisit


def log_user_visit(username):
	# Log user page visit
	user, _ = User.objects.get_or_create(username=username)

	try:
		user_visit_qs = UserVisit.objects.get(user=user)
		user_visit_qs.last_seen = timezone.now()
		user_visit_qs.visits += 1
		user_visit_qs.save()
	except UserVisit.DoesNotExist:
		UserVisit.objects.create(user=user, last_seen=timezone.now(), visits=1)

	return user
