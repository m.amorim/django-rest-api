from rest_framework import serializers

from bunnies.models import Bunny, RabbitHole


class RabbitHoleSerializer(serializers.ModelSerializer):

    bunnies = serializers.PrimaryKeyRelatedField(many=True, queryset=Bunny.objects.all())
    bunny_count = serializers.SerializerMethodField()

    def get_bunny_count(self, obj):
        return Bunny.objects.filter(home=obj).count()

    class Meta:
        model = RabbitHole
        fields = ('location', 'bunnies', 'bunny_count', 'owner')


class BunnySerializer(serializers.ModelSerializer):

    home = serializers.SlugRelatedField(queryset=RabbitHole.objects.all(), slug_field='location')
    family_members = serializers.SerializerMethodField()

    def get_family_members(self, obj):
        return [bunny.name for bunny in Bunny.objects.exclude(name=obj.name).filter(home=obj.home)]

    def validate(self, attrs):
        family_members_count = Bunny.objects.filter(home=attrs["home"]).count()
        if attrs["home"].bunnies_limit == family_members_count:
            raise serializers.ValidationError(code=400)

        return attrs

    class Meta:
        model = Bunny
        fields = ('name', 'home', 'family_members')
